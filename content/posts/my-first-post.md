---
title: "My First Post"
date: 2019-09-11T17:59:22+02:00
class: post
draft: false
---

Welcome to my new blog, this is still an experiment to use a static site generator to manage my blog. My first blog was composed by HTML files parsed by a bash script and uploaded on the web site where I had a directory, so it's like getting back to the old days.
