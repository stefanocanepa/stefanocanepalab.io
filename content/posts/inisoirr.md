---
title: "Inis Oìrr"
date: 2020-07-12T20:51:31+01:00
draft: false
toc: false
images:
tags:
  - untagged
---

Yesterday, after almost 6 year I've move to Galway I went to the Aran Island. We
went to Inis Oìrr.

The trip was fine apart two small issues.
One is that at Ros a Mhil Pier it was not clear were we had to go to board.
In my opinion they should put in place some signal to guide
people to the right queue and avoid useless requests to crew. The other one,
a life guard, who was travelling to the island, was not wearing his face mask
until my wife looked at him so badly he put it on. It was supposed to be
compulsory on board but the crew did not enforce the company policy.

The ferry was almost empty but we did not realize until we decided to move to
the closed deck because of the wind.

The weather was beautiful and we walked, or better wander around, almost all day
enjoying the panorama and the sun. We missed to go to some of the places suggested
by the every guide or website but we were so happy to be around together that we
do not care at all.

It was a really good day, a good way to celebrate Giuliana birthday even if she
is not with us anymore.
