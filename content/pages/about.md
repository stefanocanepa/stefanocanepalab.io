---
title: "About me"
date: 2020-03-29T23:04:56+01:00
draft: false
comments: false
images:
---

The time has come to introduce myself.

As you have already certainly understood my name is Stefano Canepa and my main passions are: GNU/Linux, free software and running, cycling, and swimming.

I was born in [Genoa](https://en.wikipedia.org/wiki/Genoa) in 1970 and I lived there until September 2014 when I moved to [Galway](https://en.wikipedia.org/wiki/Galway), now it's difficult to say where home is, I'm saying I'm "going home" when I travel both ways. Galway is a so welcoming place that really I feel like I have ever been here.

I'm currently working at Hewlett Packard Enterprise doing stuff in the clouds but let's go a little bit deeper in my story.

My first computer was a [Spectravideo MSX](https://en.wikipedia.org/wiki/SVI-728), I cannot remember when that computer got home but it was amazing, I learn a little bit of basic on it. When I was at high school my father decided to buy a PC. It was a compatible one built around a 8088 processor, it has floppy, and an hard-drive. It ran some version of [MS-DOS](https://en.wikipedia.org/wiki/MS-DOS) and it was on it I learned to program in Pascal and [Clipper](<https://en.wikipedia.org/wiki/Clipper_(programming_language)>). I eventually upgraded this PC to a 286, then a 386 and then I was forced to change the case as per the power supply.

When the first version of Windows came out I switched to MacIntosh and I owned a [Classic II](https://en.wikipedia.org/wiki/Macintosh_Classic_II), a [Color Classic](https://en.wikipedia.org/wiki/Macintosh_Color_Classic) and a [PowerBook 190](https://en.wikipedia.org/wiki/PowerBook_190,_PowerBook_190cs), but at this point I was already studying at University and working.

I have been using the Internet since before everyone noticed the existence of the Net in 1994. After all, the Internet was born before me! In those days the best part of life was to read the news with [xrn](http://www.mit.edu/people/jik/software/xrn.html) and archie was considered essential to find utilities for DOS, Mac and Unix without getting lost in ftp sites. [WAIS](http://www.wikipedia.com/wiki/WAIS) seemed the best product for bibliographic research and [Gopher](http://www.wikipedia.com/wiki/Gopher+protocol) was presented as the best way to publish information in non-homogeneous formats.
Then the web came: the first few pages were mostly just text, few images, no special effects. [Mosaic](http://www.wikipedia.com/wiki/Mosaic+web+browser) was the most used browser. At that time my favorite operating system was MacOS or better Apple System 6 with Multifinder.

In 1994, after an unforgettable experience at [Networld + Interop](http://www.interop.com) in Paris, I started working for [ITnet](https://web.archive.org/web/19961018051843/http://www.it.net/), one of the first Italian Internet providers. My studies have undergone an inevitable setback but the period spent in this company, despite a thousand difficulties, is what allowed me to lay the foundations of my current knowledge of local and geographic networks, and Unix systems. During this period some friends began to work with a new free operating system, with sources in the public domain. This was how I got to know Linux. Those were the years when Microsoft brought out Windows NT. It was more or less 1996 when I installed RedHat on my PC to replace the previous experiments with [Slackware](http://www.slackware.com).

In 1997 I did my military service but I have not stopped working with computers since I spent eight months at the Information Office of the then Northwest Military Region. I took advantage of this to learn how to administer the basic functions of [Novell </a> Netware](https://en.wikipedia.org/wiki/NetWare). When I left the office they had an email server, a new UTP based network and a Linux server with MySQL.

From 1998 to 2001 I worked for the [Lloyd Italico Assicurazioni](http://www.lia.it). Don't be afraid ... I wasn't an insurer! I continued to be that nerd I'm now, halfway between the systems and network administrator, I was just wearing a suite and a tie. In this period RedHat had released version 6.1 of its distribution which, however, ... I didn't like it anymore. I started looking for an alternative by trying [Mandrake](http://www.mandrake-soft.com), [TurboLinux](http://www.turbolinux.com) and [Suse](http://www.suse.de) until [Marco D'Itri](http://www.linux.it/~md/index.html) convinced me to install [Debian](http://www.debian.org) on IBM ThinkPad 380Z. In the meantime, I also learned to program with Borland Delphi which is what inspired [Lazarus](<https://en.wikipedia.org/wiki/Lazarus_(IDE)>). </p>

From 2001 to 2003 I worked at the [Marconi](https://en.wikipedia.org/wiki/Marconi_Company) where I studied enigmatic things that are the basis of the management networks of telephone networks. I did communication protocol tests, load tests and I enjoyed making programs to load unnecessary packets on networks. I have been to England a few times where I spent a few nights stress testing devices that were still in development stage. I designed, with my colleagues, networks for the largest Italian and foreign operators.

After I got tired of designing and testing networks I moved to [Medical Systems](http://www.medicalsystems.it) to carry out an extremely ambitious project: a management program for a laboratory of analysis released as free license. The project failed due to lack of interest and difficulty in the initial analysis, but I manage to take part to the development of a new software to get data from laboratory instrument, I wrote a first prototype of software to manage an device that automatically routes samples in between different instruments and a few other small projects.

In 2008 I left Medical Systems to go and take care of software in a railway engineering firm Penta Engineering. I took part in two projects writing code in C # and C ++ even if I am not a great C++ developer.

Since September 2014 I have been working at Hewlett Packard Enterprise. I'm working with cloud based technologies, mainly OpenStack but no only.

In the hypothesis that you have read even half of what I have written so far, you are probably wondering: "This guy is always in front of a computer? Does he just like computer science and networks? Well, according to my wife, you are not far from the truth.
